package com.aksoftis.predicable.common.utils

import android.support.v7.app.AppCompatActivity

interface PermissionChecker {

    val permissions: Array<String>
    fun checkPermissions(activity: AppCompatActivity): Boolean
    fun isRequestRequired(activity: AppCompatActivity): Boolean
}