package com.aksoftis.predicable.common.utils

import android.Manifest

object LocationPermissionChecker : PermissionCheckerImpl() {

    override val permissions = arrayOf(
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION
    )
}
