package com.aksoftis.predicable.common.utils

import java.text.SimpleDateFormat
import java.util.*

object DateFormatter {

    fun format(millis: Long = System.currentTimeMillis()) =
        SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
            .format(Date(millis)).replace("/", ".")
}
