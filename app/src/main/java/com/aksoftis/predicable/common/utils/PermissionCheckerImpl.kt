package com.aksoftis.predicable.common.utils

import android.content.pm.PackageManager
import android.os.Build
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity

abstract class PermissionCheckerImpl : PermissionChecker {

    override fun checkPermissions(activity: AppCompatActivity): Boolean {
        return if (isRequestRequired(activity)) {
            ActivityCompat.requestPermissions(
                activity,
                permissions,
                1
            )
            false
        } else true
    }

    override fun isRequestRequired(activity: AppCompatActivity): Boolean {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                permissions.map {
                    ContextCompat.checkSelfPermission(
                        activity,
                        it
                    )
                }.any { it != PackageManager.PERMISSION_GRANTED }
    }


//        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&


//                ContextCompat.checkSelfPermission(
//                    activity,
//                    Manifest.permission.ACCESS_COARSE_LOCATION
//                ) != PackageManager.PERMISSION_GRANTED &&
//
//                ContextCompat.checkSelfPermission(
//                    activity,
//                    Manifest.permission.ACCESS_FINE_LOCATION
//                ) != PackageManager.PERMISSION_GRANTED
//    }
}