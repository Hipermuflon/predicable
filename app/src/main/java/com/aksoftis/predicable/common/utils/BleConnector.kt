package com.aksoftis.predicable.common.utils

import com.polidea.rxandroidble2.RxBleConnection
import com.polidea.rxandroidble2.RxBleDevice
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

object BleConnector {

    private val disconnectTriggerSubject = PublishSubject.create<Boolean>()
    private var connectionObservable : Observable<RxBleConnection>? = null
    private var currentConnection : RxBleConnection? = null

    fun getConnection(rxBleDevice: RxBleDevice): Observable<RxBleConnection> {
        return currentConnection?.let{ Observable.just(it)} ?: prepareConnection(rxBleDevice)
    }

    fun disconnect() {
        currentConnection = null
        connectionObservable = null
        disconnectTriggerSubject.onNext(true)
    }

    private fun prepareConnection(rxBleDevice: RxBleDevice): Observable<RxBleConnection> {
        return rxBleDevice
            .establishConnection(false)
            .retry(2)
            .doOnNext { currentConnection = it  }
            .takeUntil(disconnectTriggerSubject)
    }
}
