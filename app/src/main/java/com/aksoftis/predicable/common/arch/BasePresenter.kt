package com.aksoftis.predicable.common.arch

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BasePresenter {

    protected abstract val model: BaseModel

    private val disposable = CompositeDisposable()

    abstract fun onCreate()

    open fun onDestroy() {
        unsubscribe()
    }

    private fun unsubscribe() = disposable.takeIf { it.isDisposed.not() }?.dispose()

    protected fun Disposable.manage(): Disposable {
        disposable.add(this)
        return this
    }

}