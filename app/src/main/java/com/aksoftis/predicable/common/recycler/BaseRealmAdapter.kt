package com.aksoftis.predicable.common.recycler

import android.support.v7.widget.RecyclerView
import io.realm.OrderedCollectionChangeSet
import io.realm.RealmObject
import io.realm.RealmResults

abstract class BaseRealmAdapter<RealmItem : RealmObject, VH : BaseViewHolder>(
    val items: RealmResults<RealmItem>
) : RecyclerView.Adapter<VH>(){

    init{
        items.addChangeListener { _, changeSet ->
            refreshOnRealmChanges(changeSet)
        }
    }

    protected open fun refreshOnRealmChanges(changeSet: OrderedCollectionChangeSet) = notifyDataSetChanged()

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: VH, position: Int) = holder.bind(position)
}
