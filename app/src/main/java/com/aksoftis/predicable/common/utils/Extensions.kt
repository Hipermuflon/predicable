package com.aksoftis.predicable.common.utils

import android.app.Activity
import android.content.Context
import android.support.annotation.LayoutRes
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import com.polidea.rxandroidble2.RxBleConnection
import com.polidea.rxandroidble2.RxBleDevice
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


fun View.hideKeyboard(){
    (context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager)
        .hideSoftInputFromWindow(windowToken, 0)
}

fun TextView.setTextIfEmpty(springLabel: String) {
    if (text.isEmpty())
        text = springLabel
}

fun Context.createArrayFrom(arrayResId: Int): Array<String> {
    val resourceArray = resources.getStringArray(arrayResId)
    return Array(resourceArray.size) {resourceArray[it]}
}

fun <T> Single<T>.async() = this.subscribeOn(Schedulers.io())
    .observeOn(AndroidSchedulers.mainThread())

fun <T> Observable<T>.async() = this.subscribeOn(Schedulers.io())
    .observeOn(AndroidSchedulers.mainThread())

fun Completable.async() = this.subscribeOn(Schedulers.io())
    .observeOn(AndroidSchedulers.mainThread())

//fun <T, R> Iterable<T>.map(transform: (T) -> R): MutableList<R> = map(transform).toMutableList()

fun inflate(@LayoutRes layoutResId: Int, parent: ViewGroup) =
    LayoutInflater.from(parent.context).inflate(layoutResId, parent, false)

fun RxBleConnection.RxBleConnectionState.isDisconnected() =
    this == RxBleConnection.RxBleConnectionState.DISCONNECTED

fun RxBleConnection.RxBleConnectionState.isStable() = this in arrayOf(
        RxBleConnection.RxBleConnectionState.CONNECTED ,
        RxBleConnection.RxBleConnectionState.DISCONNECTED
    )





