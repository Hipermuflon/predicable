package com.aksoftis.predicable.ui.mvp

import HeartRateContract
import android.util.Log
import com.aksoftis.predicable.domain.entities.realm.HeartRateRealm
import com.aksoftis.predicable.repository.HeartRateDao
import com.aksoftis.predicable.common.utils.async
import com.polidea.rxandroidble2.RxBleClient
import com.polidea.rxandroidble2.RxBleDevice
import com.polidea.rxandroidble2.scan.ScanFilter
import com.polidea.rxandroidble2.scan.ScanSettings
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import java.util.concurrent.TimeUnit

class HeartRateModel(
    private val heartRateDao: HeartRateDao,
    private val rxBleClient: RxBleClient
) : HeartRateContract.Model {

    private var macAddress = ""

    override fun findBleDevice(): Observable<RxBleDevice> = when {
        macAddress.isNotEmpty() -> Observable.just(device)
        else -> getDeviceFromScan()
    }

    override val device get()= rxBleClient.getBleDevice(macAddress)

    override fun saveHeartRate(heartRate: String) {
        heartRateDao.save(HeartRateRealm(heartRate))
    }

    override fun getHistoricalData() =
        Single.just(heartRateDao.getSorted()).async()

    override fun clearHeartRate() = Completable.fromCallable {
        heartRateDao.deleteAll()
    }.async()

    private fun getDeviceFromScan(): Observable<RxBleDevice> {
        val scanSettings = ScanSettings.Builder()
            .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
            .setCallbackType(ScanSettings.CALLBACK_TYPE_ALL_MATCHES)
            .build()

        dcsdc()

        return rxBleClient
            .scanBleDevices(scanSettings, ScanFilter.Builder().build())
            .take(1)
            .map {
                macAddress = it.bleDevice.macAddress
                it.bleDevice
            }
            .delay(500, TimeUnit.MILLISECONDS)
    }

    private fun dcsdc(){
        rxBleClient.observeStateChanges()
            .async()
            .subscribe { Log.d("Stan",it.name) }
//            .switchMap{ state ->
////                Observable.empty<RxBleScanResult>()
//            // switchMap makes sure that if the state will change the rxBleClient.scanBleDevices() will dispose and thus end the scan
//                when (state) {
//                    READY ->
//                        // everything should work
//                        getDeviceFromScan()
//                    BLUETOOTH_NOT_AVAILABLE -> Observable.empty<RxBleScanResult>()
//                    // basically no functionality will work here
//                    LOCATION_PERMISSION_NOT_GRANTED -> Observable.empty<RxBleScanResult>()
//                    BLUETOOTH_NOT_ENABLED -> Observable.empty<RxBleScanResult>()
//                    LOCATION_SERVICES_NOT_ENABLED -> Observable.empty<RxBleScanResult>()
//                    // scanning will not work
//                    else -> Observable.empty<RxBleScanResult>()
//                }
//            }

    }


}
