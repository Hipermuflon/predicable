import com.aksoftis.predicable.common.arch.BaseModel
import com.aksoftis.predicable.domain.entities.realm.HeartRateRealm
import com.aksoftis.predicable.ui.mvp.RefreshableView
import com.polidea.rxandroidble2.RxBleDevice
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.realm.RealmResults

interface HeartRateContract {

    interface Model : BaseModel {
        val device: RxBleDevice
        fun findBleDevice(): Observable<RxBleDevice>
        fun saveHeartRate(heartRate : String)
        fun clearHeartRate() : Completable
        fun getHistoricalData() : Single<RealmResults<HeartRateRealm>>
    }

    interface View : RefreshableView {
        fun onDeviceFound(deviceInfo: String, connectionStatus: String)
        fun onConnectionFailure()
        fun onDeviceNotFound()
        fun onBluetoothOff()
        fun enableUpdate(isEnabled: Boolean)
        fun showHistoricalData(measurements: RealmResults<HeartRateRealm>)
    }

    interface Presenter {
        fun requestNewCharacteristic()
        fun readCharacteristic()
        fun invokeClear()
        fun addRandom()
    }
}