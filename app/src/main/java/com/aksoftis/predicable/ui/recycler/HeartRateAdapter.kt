package com.aksoftis.predicable.ui.recycler

import android.util.Log
import android.view.View
import android.view.ViewGroup
import com.aksoftis.predicable.R
import com.aksoftis.predicable.common.recycler.BaseRealmAdapter
import com.aksoftis.predicable.domain.entities.realm.HeartRateRealm
import com.aksoftis.predicable.common.recycler.BaseViewHolder
import com.aksoftis.predicable.common.utils.DateFormatter
import com.aksoftis.predicable.common.utils.inflate
import io.realm.OrderedCollectionChangeSet
import io.realm.RealmResults
import kotlinx.android.synthetic.main.heart_rate_item.view.*

class HeartRateAdapter(measurements: RealmResults<HeartRateRealm>)
    : BaseRealmAdapter<HeartRateRealm, HeartRateAdapter.HearRateViewHolder>(measurements) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        HearRateViewHolder(inflate(R.layout.heart_rate_item, parent))

    override fun refreshOnRealmChanges(changeSet: OrderedCollectionChangeSet) {
        when {
            changeSet.deletions.isNotEmpty() -> notifyDataSetChanged()
            changeSet.insertionRanges.isNotEmpty() -> {
                Log.d("HeartRateAdapter","added item")
                notifyItemInserted(0)
            }
        }
    }

    inner class HearRateViewHolder(itemView: View) : BaseViewHolder(itemView){
        override fun bind(position: Int) {
            itemView.run {
                val measurement = items[position]
                measurement?.registerDate?.let{ dateTV.text = DateFormatter.format(it) }
                heartRateTV.text = measurement?.heartRate
            }
        }

    }
}
