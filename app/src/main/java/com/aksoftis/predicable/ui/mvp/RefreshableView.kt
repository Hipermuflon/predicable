package com.aksoftis.predicable.ui.mvp

interface RefreshableView {
    fun setRefreshing(isRefreshing: Boolean)
}