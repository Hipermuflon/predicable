package com.aksoftis.predicable.ui.mvp

import HeartRateContract
import android.content.pm.PackageManager
import android.widget.Toast
import com.aksoftis.predicable.R
import com.aksoftis.predicable.common.arch.BaseActivity
import com.aksoftis.predicable.domain.clients.BLEClientProvider
import kotlinx.android.synthetic.main.activity_main.*
import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.View
import com.aksoftis.predicable.domain.entities.realm.HeartRateRealm
import com.aksoftis.predicable.repository.HeartRateDao
import com.aksoftis.predicable.ui.recycler.HeartRateAdapter
import io.realm.Realm
import io.realm.RealmResults
import kotlinx.android.synthetic.main.device_lay.*

class HeartRateActivity : BaseActivity(),HeartRateContract.View{

    override val layoutResId = R.layout.activity_main

    override val presenter by lazy {
        HeartRatePresenter(
            HeartRateModel( HeartRateDao,BLEClientProvider.getClient(this) ),
            this
        )
    }

    override fun onViewsBound() {
        Realm.init(this)
        updateBtn.setOnClickListener { presenter.readCharacteristic() }
        addRandomBtn.setOnClickListener { presenter.addRandom() }
        clearBtn.setOnClickListener { presenter.invokeClear() }
    }

    override fun showHistoricalData(measurements: RealmResults<HeartRateRealm>) {
        heartRateList.adapter = HeartRateAdapter(measurements).apply {
            registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
                override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                    heartRateList.scrollToPosition(0)
                }
            })
        }
    }

    override fun onDeviceFound(deviceName: String, connectionStatus: String) {
        deviceNameTV.text = deviceName
        connectionStatusTV.text = connectionStatus
    }

    override fun setRefreshing(isRefreshing: Boolean) {
        spinningLoader.visibility = if(isRefreshing) View.VISIBLE else View.GONE
    }

    override fun enableUpdate(isEnabled: Boolean){
        updateBtn.isEnabled = isEnabled
    }

    override fun onConnectionFailure() {
    }

    override fun onBluetoothOff() =
        startActivityForResult(Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE),1)

    override fun onDeviceNotFound() {
        deviceNameTV.text = getString(R.string.no_device)
        connectionStatusTV.text = ""
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.all{ it == PackageManager.PERMISSION_GRANTED })
            presenter.requestNewCharacteristic()
        else
            Toast.makeText(this,getString(R.string.location_required),Toast.LENGTH_LONG).show()
    }
}
