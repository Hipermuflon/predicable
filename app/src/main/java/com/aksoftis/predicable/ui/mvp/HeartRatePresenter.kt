package com.aksoftis.predicable.ui.mvp

import HeartRateContract
import android.support.annotation.VisibleForTesting
import android.support.annotation.VisibleForTesting.PRIVATE
import android.util.Log
import com.aksoftis.predicable.common.arch.BasePresenter
import com.aksoftis.predicable.common.utils.BleConnector
import com.aksoftis.predicable.common.utils.async
import com.aksoftis.predicable.common.utils.isDisconnected
import com.aksoftis.predicable.common.utils.isStable
import com.polidea.rxandroidble2.RxBleConnection
import com.polidea.rxandroidble2.RxBleDevice
import com.polidea.rxandroidble2.exceptions.BleAlreadyConnectedException
import io.reactivex.Observable
import java.util.*
import java.util.concurrent.TimeUnit

class HeartRatePresenter(
    override val model: HeartRateContract.Model,
    private val view: HeartRateContract.View
) : BasePresenter(), HeartRateContract.Presenter {

    private val HEART_RATE_MEASUREMENT_UUID = UUID.fromString("00002a37-0000-1000-8000-00805f9b34fb")

    override fun onCreate() {
        view.setRefreshing(true)
        requestNewCharacteristic()
        loadHistoricalData()
    }

    @VisibleForTesting(otherwise = PRIVATE)
    fun loadHistoricalData() {
        model.getHistoricalData()
            .subscribe(view::showHistoricalData) { Log("Error readCharacteristic " + it?.localizedMessage.orEmpty()) }
            .manage()
    }

    override fun addRandom() = model.saveHeartRate(
        (Random().nextInt(100) + 50).toString()
    ) // jako proof of concept, aby sprawdzić czy działaja interakcje z realmem, bo niestety za rzadko udawał się odczyt ;(

    override fun requestNewCharacteristic() {
        model.findBleDevice()
            .flatMap {
                Log("establishing connection")
                observeConnection(it)
                BleConnector.getConnection(it)
            }
            .onErrorResumeNext{ t :Throwable ->
                when (t) {
                    is BleAlreadyConnectedException -> BleConnector.getConnection(model.device)
                    else -> Observable.empty<RxBleConnection>()
                }
            }
            .flatMapSingle {it.readCharacteristic(HEART_RATE_MEASUREMENT_UUID) }
            .map{ String(it) }
            .doOnEach{
                it.value?.let(model::saveHeartRate)
            }
            .async()
            .doFinally { view.setRefreshing(false) }
            .subscribe(
                {
                },
                {

                    if(it?.localizedMessage?.contains("Bluetooth disabled") == true) {
                        view.onBluetoothOff()
                    }

                    if(it is BleAlreadyConnectedException)
                        BleConnector.disconnect()

                    view.setRefreshing(false)

                    Log(it,"Error requestNewCharacteristic")
                }
            )
            .manage()
    }

    override fun readCharacteristic(){
        view.setRefreshing(true)
        model.findBleDevice()
            .flatMap {
                Log("establishing connection")
                BleConnector.getConnection(it)
            }
            .delay(500,TimeUnit.MILLISECONDS)
            .onErrorResumeNext{t :Throwable ->
                when (t) {
                    is BleAlreadyConnectedException -> BleConnector.getConnection(model.device)
                    else -> Observable.empty<RxBleConnection>()
                }
            }
            .flatMapSingle { it.readCharacteristic(HEART_RATE_MEASUREMENT_UUID) }
            .retry(1)
            .map { String(it) }
            .doOnEach{
                it.value?.let(model::saveHeartRate)
            }
            .async()
            .doOnError { BleConnector.disconnect()}
            .doFinally { view.setRefreshing(false) }
            .subscribe({}){ Log( "Error readCharacteristic " + it?.localizedMessage.orEmpty())}
            .manage()

        // najczęściej
        // BleGattCannotStartException - podobono występuje gdy charakterystyki nie da się przeczytać
        // Wynika z błędów autoryzacji między urządzeniami
        // Niestety internet poza tym pisze tylko, że "these are known bugs on android ;(
    }

    private fun observeConnection(bleDevice: RxBleDevice) {
        bleDevice.observeConnectionStateChanges()
            .async()
            .subscribe {
                if(it.isDisconnected()) {
                    BleConnector.disconnect()
                    view.setRefreshing(false)
                }
                view.enableUpdate(it.isStable())
                view.onDeviceFound(bleDevice.name ?: bleDevice.macAddress,it.name)
            }
            .manage()
    }

    override fun invokeClear() {
        model.clearHeartRate()
            .subscribe({}) { Log(it,"Error characteristic")  }
            .manage()
    }

    private fun Log(throwable: Throwable, throwablePrefix: String = "Error") = Log.d("HeartRatePresenter","$throwablePrefix ${throwable.localizedMessage.orEmpty()}")

    private fun Log(message: String) = Log.d("HeartRatePresenter",message)

}
  