package com.aksoftis.predicable.repository

import com.aksoftis.predicable.domain.entities.realm.HeartRateRealm
import io.realm.Sort

object HeartRateDao : BaseRealmDao<HeartRateRealm>() {
    override val realmClass = HeartRateRealm::class.java

    fun getSorted()= findAllWhere {
        it.sort("registerDate",Sort.DESCENDING)
    }
}
