package com.aksoftis.predicable.repository

import io.realm.Realm
import io.realm.RealmObject
import io.realm.RealmQuery
import io.realm.RealmResults

abstract class BaseRealmDao<REALM : RealmObject> {

    private val defaultInstance : Realm get() = Realm.getDefaultInstance()
    abstract val realmClass: Class<REALM>

    private fun query(): RealmQuery<REALM> = defaultInstance.where(realmClass)
    private fun findAll(): RealmResults<REALM> = query().findAll()

    fun findAllWhere(whereConditions : (RealmQuery<REALM>) -> RealmQuery<REALM>) : RealmResults<REALM> {
        return query().let(whereConditions::invoke).findAll()
    }

    fun save(data: REALM) : Boolean = with(defaultInstance){
        try {
            executeTransaction { it.copyToRealmOrUpdate(data) }
            return true
        }
        catch (e: Exception) {
            cancelTransaction()
            return false
        }
        finally {
            close()
        }
    }

    fun deleteAll(): Boolean = with(defaultInstance){
        try {
            executeTransaction { findAll().deleteAllFromRealm() }
            return true
        }
        catch (e: Exception) {
            cancelTransaction()
            return false
        }
        finally {
            close()
        }
    }
}
