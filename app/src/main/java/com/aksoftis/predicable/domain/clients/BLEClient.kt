package com.aksoftis.predicable.domain.clients

import android.support.v7.app.AppCompatActivity
import com.polidea.rxandroidble2.RxBleClient

interface BLEClient {

    fun getClient(context: AppCompatActivity) : RxBleClient
}