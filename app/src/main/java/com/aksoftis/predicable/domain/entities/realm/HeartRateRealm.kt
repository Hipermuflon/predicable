package com.aksoftis.predicable.domain.entities.realm

import io.realm.RealmObject
import io.realm.annotations.Index
import io.realm.annotations.PrimaryKey

open class HeartRateRealm(
    var heartRate: String = "",
    @PrimaryKey @Index var registerDate: Long = System.currentTimeMillis()
) : RealmObject()
