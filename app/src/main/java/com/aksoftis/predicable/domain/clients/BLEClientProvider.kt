package com.aksoftis.predicable.domain.clients

import android.support.v7.app.AppCompatActivity
import com.aksoftis.predicable.common.utils.LocationPermissionChecker
import com.polidea.rxandroidble2.RxBleClient

object BLEClientProvider : BLEClient {

    private lateinit var bleClient: RxBleClient

    override fun getClient(context: AppCompatActivity): RxBleClient {
        if(::bleClient.isInitialized.not()) {
            LocationPermissionChecker.checkPermissions(context)
            bleClient = RxBleClient.create(context)
        }

        return bleClient
    }
}
