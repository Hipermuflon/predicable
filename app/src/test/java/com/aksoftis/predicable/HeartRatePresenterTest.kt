package com.aksoftis.predicable

import com.aksoftis.predicable.domain.entities.realm.HeartRateRealm
import com.aksoftis.predicable.ui.mvp.HeartRateModel
import com.aksoftis.predicable.ui.mvp.HeartRatePresenter
import com.aksoftis.predicable.repository.HeartRateDao
import com.nhaarman.mockito_kotlin.*
import com.polidea.rxandroidble2.RxBleClient
import io.reactivex.Single
import io.realm.*
import io.realm.log.RealmLog
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.powermock.api.mockito.PowerMockito
import org.powermock.modules.junit4.rule.PowerMockRule
import io.realm.internal.RealmCore
import org.junit.runner.RunWith
import org.powermock.api.mockito.PowerMockito.mockStatic
import org.powermock.core.classloader.annotations.PowerMockIgnore
import org.powermock.core.classloader.annotations.PrepareForTest
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor
import org.powermock.modules.junit4.PowerMockRunner
import org.powermock.modules.junit4.PowerMockRunnerDelegate
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.annotation.Config
import io.realm.RealmConfiguration


@RunWith(PowerMockRunner::class)
@PowerMockRunnerDelegate(RobolectricTestRunner::class)
@Config(constants = BuildConfig::class, sdk = [21])
@PowerMockIgnore("org.mockito.*", "org.robolectric.*", "android.*")
@SuppressStaticInitializationFor("io.realm.internal.Util")
@PrepareForTest(Realm::class, RealmConfiguration::class, RealmQuery::class, RealmResults::class, RealmCore::class, RealmLog::class)
class HeartRatePresenterTest : BasePresenterTest() {

    @get:Rule
    val rule = PowerMockRule()
    private lateinit var mockRealm: Realm

    private val heartRateDao: HeartRateDao = mock()
    private val bleConnector: RxBleClient = mock()
    private val model =  HeartRateModel(heartRateDao,bleConnector)
    private val view : HeartRateContract.View = mock()

    override val presenter = HeartRatePresenter(model,view)


    @Before
    fun setup() {
        mockStatic(RealmLog::class.java)
        mockStatic(Realm::class.java)
        mockStatic(RealmResults::class.java)
        Realm.init(RuntimeEnvironment.application)

        val mockRealm = PowerMockito.mock(Realm::class.java)

        RealmCore.loadLibrary(any())

        whenever(Realm.getDefaultInstance()).thenReturn(mockRealm)


        this.mockRealm = mockRealm
    }

    @Test
    fun shouldShowHistoricalData(){
        val heartRates = mockRealmResults<HeartRateRealm>()
        val heartRateQuery = mockRealmQuery<HeartRateRealm>()
        whenever(heartRateQuery.findAll()).thenReturn(heartRates)
        whenever(model.getHistoricalData()).thenReturn(Single.just(heartRates))
        verify(view, times(1)).showHistoricalData(heartRates)
    }


    private fun <T : RealmObject> mockRealmQuery(): RealmQuery<T> {
        return mock()
    }

    private fun <T : RealmObject> mockRealmResults(): RealmResults<T> {
        return mock()
    }

}