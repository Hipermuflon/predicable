package com.aksoftis.predicable

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.aksoftis.predicable.common.arch.BasePresenter
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Rule

abstract class BasePresenterTest {

    @Before
    fun mockRxSchedulers() = RxAndroidPlugins.setInitMainThreadSchedulerHandler{ Schedulers.trampoline()}

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    abstract val presenter : BasePresenter

}